package main

import (
	"context"
	"flag"
	"html/template"
	"net/http"
	_ "net/http/pprof" // TODO: Remove on production
	"os"

	"github.com/joho/godotenv"
	"go.uber.org/zap"
)

type wsMessage struct {
	mt  int
	msg []byte
}

var addr = flag.String("addr", "localhost:3005", "http service address")
var statInterval = flag.Int("statInterval", 3000, "interval for stats info log in miliseconds (0 is never)")
var dev = flag.Bool("dev", true, "development mode")
var jwtSecret string

var logger *zap.Logger
var ctx = context.Background()

func home(w http.ResponseWriter, r *http.Request) {
	homeTemplate.Execute(w, "ws://"+r.Host+"/ws")
}

func main() {
	initLogger()
	defer logger.Sync()
	err := godotenv.Load()
	if err != nil {
		logger.Error("error-loading-env-file", zap.Error(err))
		return
	}
	jwtSecret = os.Getenv("JWT_SECRET")
	if jwtSecret == "" {
		logger.Error("invalid-jwt-secret")
		return
	}
	flag.Parse()
	initRedis()
	initWsUpgrader()

	logger.Info("starting", zap.String("address", *addr))
	subscribeToChannel()

	// startUserConnectionsLogger()
	startUserConnectionsService()

	startIncomingBackendMessagesService()
	startBackendNotificationsService()

	// startWerkConnectionsLogger()
	startWerkConnectionsService()

	http.HandleFunc("/ws", upgrade)
	http.HandleFunc("/", home)
	err = http.ListenAndServe(*addr, nil)
	logger.Fatal("Crashed", zap.Error(err))
}

func initLogger() {
	var err error
	logger, err = zap.NewDevelopment()
	if err != nil {
		panic(err)
	}
}

var homeTemplate = template.Must(template.New("").Parse(`
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<script>  
window.addEventListener("load", function(evt) {
    var output = document.getElementById("output");
    var input = document.getElementById("input");
    var ws;
    var print = function(message) {
        var d = document.createElement("div");
        d.textContent = message;
        output.appendChild(d);
    };
    document.getElementById("open").onclick = function(evt) {
        if (ws) {
            return false;
        }
        ws = new WebSocket("{{.}}");
        ws.onopen = function(evt) {
            ws.send(JSON.stringify({ token: 'TestPepeg' }));
            print("OPEN");
        }
        ws.onclose = function(evt) {
            print("CLOSE");
            ws = null;
        }
        ws.onmessage = function(evt) {
            print("RESPONSE: " + evt.data);
        }
        ws.onerror = function(evt) {
            print("ERROR: " + evt.data);
        }
        return false;
    };
    document.getElementById("send").onclick = function(evt) {
        if (!ws) {
            return false;
        }
        print("SEND: " + input.value);
        ws.send(input.value);
        return false;
    };
    document.getElementById("close").onclick = function(evt) {
        if (!ws) {
            return false;
        }
        ws.close();
        return false;
    };
});
</script>
</head>
<body>
<table>
<tr><td valign="top" width="50%">
<p>Click "Open" to create a connection to the server, 
"Send" to send a message to the server and "Close" to close the connection. 
You can change the message and send multiple times.
<p>
<form>
<button id="open">Open</button>
<button id="close">Close</button>
<p><input id="input" type="text" value="Hello world!">
<button id="send">Send</button>
</form>
</td><td valign="top" width="50%">
<div id="output"></div>
</td></tr></table>
</body>
</html>
`))
