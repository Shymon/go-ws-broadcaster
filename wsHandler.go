package main

import (
	"flag"
	"net/http"

	"github.com/Shymon/werkflow/broadcaster/werks"
	"github.com/gorilla/websocket"
	"go.uber.org/zap"
)

var upgrader = websocket.Upgrader{} // use default options
var authTimeout = flag.Int("authTimeout", 5000, "timeout for receiving authentication message")

const (
	minTokenLength = 4
	maxTokenLength = 1024
)

func initWsUpgrader() {
	if *dev {
		upgrader.CheckOrigin = func(r *http.Request) bool { return true }
	}
}

func upgrade(w http.ResponseWriter, r *http.Request) {
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		logger.Warn("upgrade-error", zap.Error(err))
		return
	}
	defer c.Close()
	user, success, err := authenticate(c)
	if err != nil || !success {
		logger.Debug("authentication-error", zap.Error(err))
		return
	}

	messagesToSend := make(chan wsMessage, 5)
	done := make(chan bool)
	_userConnection := userConnection{user, c, messagesToSend, map[werks.WerkID]bool{}}
	connectC <- _userConnection
	defer func() {
		disconnectC <- _userConnection
	}()

	startHandlingIncomingUserMessages(&_userConnection, done)
	blockOnSendingUserMessages(c, done, messagesToSend)
}

func blockOnSendingUserMessages(c *websocket.Conn, done <-chan bool, messagesToSend <-chan wsMessage) {
	for {
		select {
		case <-done:
			return
		case message := <-messagesToSend:
			err := c.WriteMessage(message.mt, message.msg)
			if err != nil {
				logger.Warn("send-error", zap.Error(err))
				if err == websocket.ErrCloseSent {
					return
				}
			}
		}
	}
}

func startHandlingIncomingUserMessages(_userConnection *userConnection, done chan<- bool) {
	go func() {
		for {
			message, err := readUserMessage(_userConnection.connection)
			if err != nil {
				break
			}

			handleIncomingMessage(&message, _userConnection)
		}
		done <- true
	}()
}

func readRawUserMessage(c *websocket.Conn) (_wsMessage wsMessage, err error) {
	mt, msg, err := c.ReadMessage()
	if err != nil {
		if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure, websocket.CloseNoStatusReceived) {
			logger.Warn("read-error", zap.Error(err))
			return
		}
		return
	}
	_wsMessage.mt = mt
	_wsMessage.msg = msg
	return
}
