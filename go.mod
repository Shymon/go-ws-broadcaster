module github.com/Shymon/werkflow/broadcaster

go 1.16

require (
	github.com/cosmtrek/air v1.26.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-redis/redis/v8 v8.7.1
	github.com/gorilla/websocket v1.4.2
	github.com/joho/godotenv v1.3.0
	github.com/pelletier/go-toml v1.9.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0
	golang.org/x/sys v0.0.0-20210403161142-5e06dd20ab57 // indirect
)
