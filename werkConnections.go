package main

import (
	"encoding/json"
	"time"

	"github.com/Shymon/werkflow/broadcaster/users"
	"github.com/Shymon/werkflow/broadcaster/werks"
	"github.com/gorilla/websocket"
	"go.uber.org/zap"
)

type werkUser struct {
	WerkID         werks.WerkID
	UserConnection *userConnection
}

var connectionsByWerk = make(map[werks.WerkID]map[*websocket.Conn]*userConnection)

var connectToWerkC = make(chan werkUser, 100)
var disconnectFromWerkC = make(chan werkUser, 100)

func startWerkConnectionsService() {
	go func() {
		for {
			select {
			case _werkUser := <-connectToWerkC:
				if connectionsByWerk[_werkUser.WerkID] == nil {
					connectionsByWerk[_werkUser.WerkID] = map[*websocket.Conn]*userConnection{}
				}

				if userNotConnectedOnOnyConnection(connectionsByWerk[_werkUser.WerkID], _werkUser.UserConnection.user.UUID) {
					notifyUsersOnWerkThatSomeoneConnected(&_werkUser)
				}

				connectionsByWerk[_werkUser.WerkID][_werkUser.UserConnection.connection] = _werkUser.UserConnection
				_werkUser.UserConnection.werks[_werkUser.WerkID] = true
			case _werkUser := <-disconnectFromWerkC:
				delete(connectionsByWerk[_werkUser.WerkID], _werkUser.UserConnection.connection)
				delete(_werkUser.UserConnection.werks, _werkUser.WerkID)

				if userNotConnectedOnOnyConnection(connectionsByWerk[_werkUser.WerkID], _werkUser.UserConnection.user.UUID) {
					notifyUsersOnWerkThatSomeoneDisconnected(&_werkUser)
				}
			}
		}
	}()
}

func userNotConnectedOnOnyConnection(werkMap map[*websocket.Conn]*userConnection, userID users.UserID) bool {
	for _, userConnection := range werkMap {
		if userConnection.user.UUID == userID {
			return false
		}
	}

	return true
}

func notifyUsersOnWerkThatSomeoneConnected(_werkUser *werkUser) {
	notificationMessage, _ := json.Marshal(userWerkStateUpdateMessage{
		messageToUserUserConnectedToWerk,
		_werkUser.WerkID,
		_werkUser.UserConnection.user.UUID,
	})
	for _, _userConnection := range connectionsByWerk[_werkUser.WerkID] {
		_userConnection.messagesChannel <- wsMessage{
			websocket.TextMessage,
			notificationMessage,
		}
	}
}

func notifyUsersOnWerkThatSomeoneDisconnected(_werkUser *werkUser) {
	notificationMessage, _ := json.Marshal(userWerkStateUpdateMessage{
		messageToUserUserDisconnectedFromWerk,
		_werkUser.WerkID,
		_werkUser.UserConnection.user.UUID,
	})
	for _, _userConnection := range connectionsByWerk[_werkUser.WerkID] {
		_userConnection.messagesChannel <- wsMessage{
			websocket.TextMessage,
			notificationMessage,
		}
	}
}

func usersOnWerk(_werkID werks.WerkID, currentUser *users.User) (werkUsersArray *[]users.User, err error) {
	werkUsersMap := make(map[users.UserID]users.User)

	for _, _userConnection := range connectionsByWerk[_werkID] {
		if _, ok := werkUsersMap[_userConnection.user.UUID]; ok {
			continue
		}

		werkUsersMap[_userConnection.user.UUID] = _userConnection.user
	}

	_werkUsersArray := []users.User{}
	for _, user := range werkUsersMap {
		if user.UUID != currentUser.UUID {
			_werkUsersArray = append(_werkUsersArray, user)
		}
	}
	werkUsersArray = &_werkUsersArray
	return
}

func startWerkConnectionsLogger() {
	if *statInterval == 0 {
		return
	}
	ticker := time.NewTicker(time.Duration(*statInterval) * time.Millisecond)

	go func() {
		for {
			select {
			case <-ticker.C:
				userSum := 0
				for _, connections := range connectionsByWerk {
					userSum += len(connections)
				}

				logger.Info("werk-connections-logger",
					zap.Int("connected-users", userSum),
					zap.Int("connect-channel-size", len(connectToWerkC)),
					zap.Int("disconnect-channel-size", len(disconnectFromWerkC)),
				)
			}
		}
	}()
}
