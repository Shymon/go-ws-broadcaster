package main

import (
	"time"

	"github.com/Shymon/werkflow/broadcaster/users"
	"github.com/Shymon/werkflow/broadcaster/werks"
	"github.com/gorilla/websocket"
	"go.uber.org/zap"
)

type userConnection struct {
	user            users.User
	connection      *websocket.Conn
	messagesChannel chan wsMessage
	werks           map[werks.WerkID]bool
}

var connectionsByUsers = make(map[users.UserID]map[*websocket.Conn]userConnection)

var connectC = make(chan userConnection, 100)
var disconnectC = make(chan userConnection, 100)

func startUserConnectionsService() {
	go func() {
		for {
			select {
			case _userConnection := <-connectC:
				if connectionsByUsers[_userConnection.user.UUID] == nil {
					connectionsByUsers[_userConnection.user.UUID] = map[*websocket.Conn]userConnection{}
				}
				connectionsByUsers[_userConnection.user.UUID][_userConnection.connection] = _userConnection
			case _userConnection := <-disconnectC:
				delete(connectionsByUsers[_userConnection.user.UUID], _userConnection.connection)
				for _werkID := range _userConnection.werks {
					disconnectFromWerkC <- werkUser{_werkID, &_userConnection}
				}
			}
		}
	}()
}

func sendToAll(message wsMessage) {
	for _, connections := range connectionsByUsers {
		for _, _userConnection := range connections {
			_userConnection.messagesChannel <- message
		}
	}
}

func startUserConnectionsLogger() {
	if *statInterval == 0 {
		return
	}
	ticker := time.NewTicker(time.Duration(*statInterval) * time.Millisecond)

	go func() {
		for {
			select {
			case <-ticker.C:
				userSum := 0
				for _, connections := range connectionsByUsers {
					userSum += len(connections)
				}

				logger.Info("user-connections-logger",
					zap.Int("connected-users", userSum),
					zap.Int("connect-channel-size", len(connectC)),
					zap.Int("disconnect-channel-size", len(disconnectC)),
				)
			}
		}
	}()
}
