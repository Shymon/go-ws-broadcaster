package main

import (
	"encoding/json"

	"github.com/Shymon/werkflow/broadcaster/users"
	"github.com/Shymon/werkflow/broadcaster/werks"
)

type userWerkStateUpdateMessage struct {
	Type   messageToUserType `json:"type"`
	WerkID werks.WerkID      `json:"werkId"`
	UserID users.UserID      `json:"userId"`
}

type userMessage struct {
	Type    userMessageType `json:"type"`
	Content json.RawMessage `json:"content"`
}

type userMessageType string
type messageToUserType string

type messageWithWerkUsers struct {
	Type  messageToUserType `json:"type"`
	Users []users.User      `json:"users"`
}

const (
	messageToUserUserConnectedToWerk      messageToUserType = "UCTW"
	messageToUserUserDisconnectedFromWerk messageToUserType = "UDFW"

	userMessageConnectedToWerk      userMessageType   = "CTW"
	userMessageDisconnectedFromWerk userMessageType   = "DFW"
	messageToUserUserWerks          messageToUserType = "WU"
)
