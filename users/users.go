package users

// UserID is user id (uuid) type
type UserID string

// User coming from frontend application
type User struct {
	UUID UserID `json:"id"`
}
