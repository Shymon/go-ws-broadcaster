package main

import "encoding/json"

const (
	backendNotificationMessageType = "N"

	backendNotificationRecipientsUsers = "users"
	backendNotificationRecipientsWerk  = "werk"
)

var backendNotificationsC = make(chan backendIncomingMessage, 5)

type backendIncomingMessage struct {
	Type           string          `json:"type"`
	RecipientsType string          `json:"recipients_type"`
	Recipients     []string        `json:"recipients"`
	Content        json.RawMessage `json:"content"`
}
