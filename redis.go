package main

import (
	"flag"

	"github.com/go-redis/redis/v8"
	"go.uber.org/zap"
)

var channelName = flag.String("channelName", "werkflow-broadcast", "redis channel name that publishes messages")
var redisAddress = flag.String("redisAddress", "localhost:6379", "redis addres in form host:port")
var redisPassword = flag.String("redisPassword", "", "redis password")
var redisDatabase = flag.Int("redisDatabase", 0, "redis database")

var rdb *redis.Client
var subscription *redis.PubSub

func initRedis() {
	rdb = redis.NewClient(&redis.Options{
		Addr:     *redisAddress,
		Password: *redisPassword, // no password set
		DB:       *redisDatabase, // use default DB
	})
}

func subscribeToChannel() {
	subscription = rdb.Subscribe(ctx, *channelName)

	// Wait for confirmation that subscription is created before publishing anything.
	_, err := subscription.Receive(ctx)
	if err != nil {
		panic(err)
	}
	logger.Info("subscribed", zap.String("channel-name", *channelName))
}
