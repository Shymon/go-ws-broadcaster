package main

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/Shymon/werkflow/broadcaster/werks"
	"github.com/gorilla/websocket"
)

func handleIncomingMessage(message *userMessage, _userConnection *userConnection) error {
	// userMessageConnectedToWerk
	if message.Type == userMessageConnectedToWerk {
		var stringID string
		json.Unmarshal(message.Content, &stringID)
		_werkID := werks.WerkID(stringID)
		if len(_werkID) < 4 && len(_werkID) > 64 {
			return errors.New("Bad message")
		}

		connectToWerkC <- werkUser{_werkID, _userConnection}
		werkUsersArray, err := usersOnWerk(_werkID, &_userConnection.user)
		if err != nil {
			return errors.New("Bad message")
		}
		byteMessage, err := json.Marshal(messageWithWerkUsers{messageToUserUserWerks, *werkUsersArray})

		_userConnection.messagesChannel <- wsMessage{
			websocket.TextMessage,
			byteMessage,
		}
		return nil
	}

	// userMessageDisconnectedFromWerk
	if message.Type == userMessageDisconnectedFromWerk {
		_werkID := werks.WerkID(message.Content)
		if len(_werkID) < 4 && len(_werkID) > 64 {
			return errors.New("Bad message")
		}

		disconnectFromWerkC <- werkUser{_werkID, _userConnection}
		return nil
	}
	return nil
}

func readUserMessage(c *websocket.Conn) (_userMessage userMessage, err error) {
	_wsMessage, err := readRawUserMessage(c)
	if err != nil {
		return
	}

	if _wsMessage.mt != websocket.TextMessage {
		err = errors.New("Invalid message type: " + fmt.Sprint(_wsMessage.mt))
		return
	}

	err = json.Unmarshal(_wsMessage.msg, &_userMessage)
	if err != nil {
		return
	}
	return
}
