package main

import (
	"encoding/json"

	"github.com/Shymon/werkflow/broadcaster/users"
	"github.com/Shymon/werkflow/broadcaster/werks"
	"github.com/gorilla/websocket"
	"go.uber.org/zap"
)

func startIncomingBackendMessagesService() {
	go func() {
		var msg backendIncomingMessage
		for {
			rawMsg := <-subscription.Channel()
			if err := json.Unmarshal([]byte(rawMsg.Payload), &msg); err != nil {
				logger.Error(
					"parsing-incoming-message-error",
					zap.Error(err),
					zap.String("channel", rawMsg.Channel),
					zap.String("message", rawMsg.Payload),
				)
				continue
			}
			if msg.Type == backendNotificationMessageType {
				backendNotificationsC <- msg
				continue
			}
		}
	}()
}

func startBackendNotificationsService() {
	go func() {
		for {
			msg := <-backendNotificationsC

			if msg.RecipientsType == backendNotificationRecipientsUsers {
				for _, recipient := range msg.Recipients {
					for _, _userConnection := range connectionsByUsers[users.UserID(recipient)] {
						_userConnection.messagesChannel <- wsMessage{websocket.TextMessage, msg.Content}
					}
				}
				continue
			}

			if msg.RecipientsType == backendNotificationRecipientsWerk {
				for _, _userConnection := range connectionsByWerk[werks.WerkID(msg.Recipients[0])] {
					_userConnection.messagesChannel <- wsMessage{websocket.TextMessage, msg.Content}
				}
				continue
			}

			logger.Error("unhandled-notification-recipient-type", zap.String("recipient-type", msg.RecipientsType))
		}
	}()
}
