package main

import (
	"encoding/json"
	"errors"
	"time"

	"github.com/Shymon/werkflow/broadcaster/users"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/websocket"
	"go.uber.org/zap"
)

type authenticationMessage struct {
	Token string `json:"token"`
}

func authenticate(c *websocket.Conn) (user users.User, success bool, err error) {
	success = false
	authMsgC := make(chan wsMessage, 1)

	go func() {
		msg, err := readRawUserMessage(c)
		if err != nil {
			logger.Debug("authentication err", zap.Error(err))
			return
		}
		authMsgC <- msg
	}()

	select {
	case message := <-authMsgC:
		var authMessage authenticationMessage
		// Unmarshalling json
		err = json.Unmarshal(message.msg, &authMessage)
		if err != nil {
			logger.Debug("Invalid authentication message format",
				zap.Error(err),
				zap.String("auth message", string(message.msg)))
			return
		}
		// Validating authToken length
		authToken := authMessage.Token
		if len(authToken) < minTokenLength || len(authToken) > maxTokenLength {
			err = errors.New("Invalid token")
			logger.Debug("invalid-token", zap.Error(err))
			return
		}
		// Parsing token
		var parsedToken *jwt.Token
		parsedToken, err = jwt.Parse(authToken, func(token *jwt.Token) (interface{}, error) {
			// Checking sign method
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, errors.New("Invalid signing method")
			}
			return []byte(jwtSecret), nil
		})
		// Checking if token is valid
		if err != nil || parsedToken == nil || !parsedToken.Valid {
			logger.Debug("invalid-token", zap.Error(err))
			return
		}

		if claims, ok := parsedToken.Claims.(jwt.MapClaims); ok {
			// Getting and checking user_id claim
			data, ok := claims["data"].(map[string]interface{})
			if !ok {
				logger.Debug("invalid-token-data-claim")
				return
			}
			uuid, ok := data["user_id"].(string)
			if !ok {
				logger.Debug("invalid-token-user-id-claim")
				return
			}
			// Assigning user
			user = users.User{UUID: users.UserID(uuid)}
			success = true
			logger.Debug("user-authenticated", zap.String("user-id", string(user.UUID)))
		}

	case <-time.After(time.Duration(*authTimeout) * time.Millisecond):
		logger.Debug("authentication timed out")
		return
	}
	return
}
